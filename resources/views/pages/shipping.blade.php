@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Envíos
                            <span>Somos tu solución rápida y segura</span></h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content" class="no-bottom">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box-container">
                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0">
                            <div class="box-with-icon-left">
                                <i class="fa fa-check-circle icon-big"></i>
                                <div class="text">
                                    <h2>Regístrate!</h2>
                                    <p>Crea tu cuenta hoy y con ella tu propio casillero gratuito. Este es tu espacio virtual para que puedas enviar tus compras y así nosotros podremos estar listos para recibir tus paquetes. Una vez registrado, te enviaremos la información que debes utilizar en la dirección de envio de tus paquetes.</p>
                                    <a href="/register" class="btn-text">Crea tu cuenta hoy!</a>
                                </div>
                            </div>
                        </div>

                        <div class="divider-double"></div>

                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0">
                            <div class="box-with-icon-left">
                                <i class="fa fa-check-circle icon-big"></i>
                                <div class="text">
                                    <h2>Comienza a hacer tus compras!</h2>
                                    <p>Una vez recibas los datos de tu casillero, puedes comenzar a comprar y a hacer tus envios de acuerdo con las instrucciones suministradas.</p>
                                    {{--<div class="divider-single"></div>--}}
                                    {{--<a href="service-details-2.html" class="btn-text">View Details</a>--}}
                                </div>
                            </div>
                        </div>

                        <div class="divider-double"></div>

                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0">
                            <div class="box-with-icon-left">
                                <i class="fa fa-cubes icon-big"></i>
                                <div class="text">
                                    <h2>Recibe notificaciones tan pronto como recibamos tus envios!</h2>
                                    <p>Una vez el servicio de transporte de tu tienda realice el envio, comenzarás a recibir nuestras notificaciones para que puedas saber como se ejecuta el tránsito de tu paquete.</p>
                                    {{--<div class="divider-single"></div>--}}
                                    {{--<a href="service-details-3.html" class="btn-text">View Details</a>--}}
                                </div>
                            </div>
                        </div>

                        <div class="divider-double"></div>

                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0">
                            <div class="box-with-icon-left">
                                <i class="fa fa-train icon-big"></i>
                                <div class="text">
                                    <h2>Calcula el costo de tu envío!</h2>
                                    <p>Nuestro sistema de costos funciona basado en el peso de tus paquetes. El costo de envío por libra es de $1.70 más 30% del valor declarado. La tarifa mínima es el precio de 15 libras y puedes enviar con nuestro servicio hasta un máximo de 110 lbs por envío.</p>
                                    <p>Una vez calculado el costo de envío, recibiras una notificación para que puedas hacer tu pago con cualquiera de nuestros métodos de pago.</p>
                                    {{--<div class="divider-single"></div>--}}
                                    {{--<a href="service-details-1.html" class="btn-text">View Details</a>--}}
                                </div>
                            </div>
                        </div>

                        <div class="divider-double"></div>

                        <div class="col-md-12 wow fadeInUp" data-wow-delay="0">
                            <div class="box-with-icon-left">
                                <i class="fa fa-home icon-big"></i>
                                <div class="text">
                                    <h2>Tiempo de Entrega</h2>
                                    <p>El tiempo promedio de entrega es entre 5 y 7 días hábiles. Tus envíos llegarán a la dirección que registráste a través de Coordinadora Mercantil ó TCC.</p>
                                    {{--<div class="divider-single"></div>--}}
                                    {{--<a href="service-details-2.html" class="btn-text">View Details</a>--}}
                                </div>
                            </div>
                        </div>

                        <div class="divider-double"></div>


                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>

        <div class="divider"></div>

    @endsection