@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="page-track no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Rastreo de Paquetes
                            <span>Monitorea tus paquetes en línea</span>
                        </h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content"  class="no-padding">
        <!-- section begin -->
        <section id="section-tracking">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <form action="">
                            <div class="cta-form wow fadeIn" data-wow-delay="0s" data-wow-duration="1s">
                                <input type="text" name="track" value="" placeholder="Ingresa tu número de guía..." required >
                                <input type="submit" value="RASTREAR PAQUETE">
                                <div class="clearfix"></div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            @if( !empty($data) )

            <div id="section-tracking-result" class="light-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="divider-double"></div>
                            <div class="text-center">
                                <h3><span class="grey">Guía:</span><br />{{ $_GET['track'] }}</h3>
                                <h3><span class="grey">Estado:</span><br />{{ $statuses[$data->status] }}</h3>
                            </div>

                            <div class="divider-double"></div>

                            <div class="wrapper-line padding40 rounded10">

                                {{--<ul class="progress">--}}
                                    {{--<li><a href="">Accepted</a></li>--}}
                                    {{--<li class="beforeactive"><a href="">Order Processing</a></li>--}}
                                    {{--<li class="active"><a href="">Shipment Pending</a></li>--}}
                                    {{--<li><a href="">Estimated Delivery</a></li>--}}
                                {{--</ul>--}}

                                {{--<div class="divider-double"></div>--}}

                                <ul class="timeline custom-tl">

                                    @if($data->status == 'delivered')

                                    <li class="timeline-inverted">
                                        <div class="timeline-date wow zoomIn" data-wow-delay=".2s">
                                            {{ date("M d, Y", substr($data->receivedOn, 0, 10)) }}
                                            <span>{{ date("h:i A", substr($data->receivedOn, 0, 10)) }}</span>
                                        </div>
                                        <div class="timeline-badge success"><i class="fa fa-check-square-o wow zoomIn"></i></div>
                                        <div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">
                                            <div class="timeline-body">
                                                {{ $statuses[$data->status] }}
                                                <span class="location">{{ $data->receivedAt }}</span>
                                            </div>
                                        </div>
                                    </li>

                                    @endif

                                    @foreach( $data->events as $event )

                                    <li class="timeline-inverted">
                                        <div class="timeline-date wow zoomIn" data-wow-delay=".2s">
                                            {{ date("M d, Y", substr($event->occurredOn, 0, 10)) }}
                                            <span>{{ date("h:i A", substr($event->occurredOn, 0, 10)) }}</span>
                                        </div>
                                        <div class="timeline-badge"><i class="fa fa-plane wow zoomIn"></i></div>
                                        <div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">
                                            <div class="timeline-body">
                                                {{ $statuses[$event->type] }}
                                                <span class="location">{{ $event->location }}</span>
                                            </div>
                                        </div>
                                    </li>

                                    @endforeach

                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endif

        </section>
        <!-- section close -->
    </div>
    <!-- content close -->

    @endsection