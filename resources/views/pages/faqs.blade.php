@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Preguntas Frecuentes
                            <span>Garantizamos siempre tu satisfacción</span>
                        </h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content" class="no-padding">

        <p>&nbsp;</p>

        <div class="container">

            @foreach($faqs as $faq)

            <div class="panel panel-info">
                <div class="panel-heading">{{ $faq['question'] }}</div>
                <div class="panel-body">
                    {!! $faq['answer'] !!}
                </div>
            </div>

            @endforeach

        </div>

    </div>
    <!-- content close -->

    @endsection