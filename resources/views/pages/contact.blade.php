@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="page-contact no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h1>Contáctanos
                            <span>Déjanos saber como podemos ayudarte</span>
                        </h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <!-- content begin -->
    <div id="content" @if( \Illuminate\Support\Facades\Request::url() === action('PagesController@index') ) class="no-padding" @endif>

        <div class="container">
            <div class="row no-gutter">
                <div class="col-md-6">
                    <div id="map"></div>
                </div>

                <div class="col-md-6">
                    <div id="contact-form-wrapper">
                        <div class="contact_form_holder">
                            <form class="row" method="post" action="{{ action('FormsController@contact') }}">
                                {{ csrf_field() }}
                                <input type="text" class="form-control" name="name" placeholder="Nombre" />
                                <input type="text" class="form-control" name="phone" placeholder="Teléfono" />
                                <input type="text" class="form-control" name="email" placeholder="Email" />
                                <textarea cols="10" rows="10" name="message" id="message" class="form-control" placeholder="Escribe tu mensaje..."></textarea>
                                <p id="btnsubmit">
                                    <input type="submit" value="Enviar Mensaje" class="btn btn-custom" />
                                </p>
                            </form>
                        </div>
                    </div>
                </div>


            </div>

            <div class="divider-line"></div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration=".8s">Sedes Costa Courier
                    </h2>
                    <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                </div>

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="col-md-4">
                            <h3>Estados Unidos</h3>
                            5502 NW 79 Av.<br />
                            Doral, FL 33166 - 4124<br />
                            Tel. +1 305-6299595<br />
                            WhatsApp. +1 754-2046280<br />
                            Horas de Atención<br />
                            Lunes a Viernes: 9:00AM a 6:00PM<br />
                            Sábado: 9:00AM a 3:00PM<br>

                            <div class="divider-single"></div>

                            {{--<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="btn-border popup-gmaps">View Location</a>--}}
                        </div>

                        <div class="col-md-4">
                            <h3>Bogotá</h3>
                            Carlos Leonardo Gonzalez<br />
                            Olga Reyes<br />
                            Calle 25B # 85 C-41<br />
                            Barrio Modelia<br />
                            Tel. 410 22 76<br />

                            <div class="divider-single"></div>

                            {{--<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="btn-border popup-gmaps">View Location</a>--}}
                        </div>

                        <div class="col-md-4">
                            <h3>Cúcuta</h3>
                            Pilar I. Lino<br />
                            Av 7C #6-06 Prados del Este<br />
                            Tel. 576 9218<br />

                            <div class="divider-single"></div>

                            {{--<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="btn-border popup-gmaps">View Location</a>--}}
                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>

    @endsection