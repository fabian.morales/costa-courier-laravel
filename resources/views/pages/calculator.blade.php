@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Calculadora de Envíos
                            <span>Tu opción más competitiva y confiable</span>
                        </h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content" class="no-padding">

        <p>&nbsp;</p>

        <div class="container">

        @if($total > 0)
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-info">
                    <div class="panel-heading">Resultado</div>
                    <div class="panel-body">
                        <p class="lead text-center">
                            El valor total de tu envío es de:
                        </p>
                        <h2 class="text-center">
                            USD {{ number_format($total, 2) }}
                        </h2>
                    </div>
                </div>
                <div class="alert alert-warning">
                    <i class="fa fa-exclamation-triangle"></i> IMPORTANTE:<br />
                    <small>
                        El valor de la Calculadora de Envíos no es una cotización oficial de Costa Courier. Considérese como un valor simulado que le permitirá calcular la tarifa de envío a Colombia. Se genera tomando cada libra del paquete de envío por $2.3 dólares que incluye cargos e impuestos. Adicional un 3% del valor asegurado para productos de más de 200 dolores. Lo invitamos a visitar el listado de artículos no permitidos para envíos vía Courier por las autoridades y las normativas actuales.
                    </small>
                </div>
                <p class="text-center">
                    <a href="{{ action('PagesController@calculator') }}" class="btn btn-default">
                        Volver
                    </a>
                </p>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="" id="calculator-form" >
                    {{ csrf_field() }}
                    <p class="lead text-center">
                        Conoce el costo de tu envío en dos simples pasos:
                    </p>
                    <div class="form-group">
                        <label for="lbs"># Libras</label>
                        <input type="number" class="form-control input-lg text-center" name="lbs" required >
                    </div>
                    <div class="form-group">
                        <label for="amount">Monto Asegurado (Opcional)</label>
                        <input type="number" class="form-control input-lg text-center" name="amount" >
                    </div>
                    <button class="btn btn-success btn-lg">
                        <i class="fa fa-calculator"></i> Calcular
                    </button>
                </form>
            </div>
        </div>
        @endif

        </div>

    </div>
    <!-- content close -->

    @endsection