@extends('layouts.front')

@section('content')

    <!-- subheader begin -->
    <section id="subheader" class="page-news no-bottom" data-stellar-background-ratio="0.5">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h1>
                            {{ $post->title }}
                        </h1>
                        <div class="small-border wow flipInY" data-wow-delay=".8s" data-wow-duration=".8s"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- subheader close -->

    <div class="clearfix"></div>

    <!-- content begin -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div id="newslist" class="news-list">

                        <div class="col-md-12 news-item item">
                            <img src="{{ $post->image }}" class="img-responsive" alt="{{ $post->title }}">
                            <div class="desc">
                                {!! $post->content !!}
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    {{--<div class="text-center ">--}}
                        {{--<ul class="pagination">--}}
                            {{--<li><a href="#">Prev</a></li>--}}
                            {{--<li class="active"><a href="#">1</a></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li><a href="#">4</a></li>--}}
                            {{--<li><a href="#">5</a></li>--}}
                            {{--<li><a href="#">Next</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                </div>



                <div id="sidebar" class="col-md-4">

                    <div class="widget widget-text">
                        <h3>Acerca de Costa Courier</h3>
                        Haz tus compras online y cuenta con nosotros para poner tu mercancia en el lugar que necesites.


                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- content close -->

    @endsection