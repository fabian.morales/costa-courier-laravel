@extends('layouts.front')

@section('content')

    <!-- section begin -->
    <section id="section-intro-2" class="no-padding autoheight light-text let-it-snow" data-stellar-background-ratio="0.5">
        <div class="center-y">
            <div class="inner text-center">
                <div class="sub-intro-text"><span class="text-white">Tu lo compras, nosotros te lo traemos.</span></div>
                <div class="divider-double"></div>
                <div class="type-wrap title big">
                    <div class="typed-strings">
                        <p>Rapidez</p>
                        <p>Seguridad</p>
                        <p>Las Mejores Tarifas</p>
                    </div>
                    <span class="typed"></span>
                </div>

                <div class="divider-single"></div>
                <a href="/register" class="btn-custom">¡Crea Tu Cuenta Gratuita Hoy!</a>
                <div class="divider-double"></div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <section id="section-services" class="no-padding">
        <div class="box-container">
            <div id="bg-service-1" class="box-one-third light-text home-box">
                <div class="inner wow" data-wow-delay="0s">
                    <h2 class="wow fadeIn" data-wow-delay=".2s">Tus Tiendas Favoritas en USA</h2>
                    <p class="wow fadeIn" data-wow-delay=".3s">Realiza tus compras en los lugares online de tu preferencia, escoge la manera de que los entreguen en Costa Courier y nosotros desde ahí nos encargamos de ponerlos en tus manos en Colombia, sin que tengas que encargarte de nada más, sólo de disfrutar tu compra.</p>
                    {{--<div class="divider-single"></div>--}}
                    {{--<a href="" class="btn-border-light wow fadeInUp" data-wow-delay=".4s" data-wow-duration=".3s">Ampliar Inform</a>--}}
                </div>
            </div>



            <div id="bg-service-3" class="box-one-third light-text home-box">
                <div class="inner">
                    <h2 class="wow fadeIn" data-wow-delay=".4s">Envío de Paquetes y Documentos</h2>
                    <p class="wow fadeIn" data-wow-delay=".5s">Para tu paquetes y documentos que deseas enviar a Colombia, de forma rápida y segura, somos tu mejor opción. Embarcamos para Colombia 3 veces a la semana, y nuestra tasa de pérdida de paquetes es de .002%, así que la posibilidad de que tu paquete se pierda prácticamente no existe, sin embargo te proveemos con un seguro sin costo adicional para tí por un máximo de $200.00.</p>
                    {{--<div class="divider-single"></div>--}}
                    {{--<a href="service-details-2.html" class="btn-border-light wow fadeInUp" data-wow-delay=".5s" data-wow-duration=".3s">Read More</a>--}}
                </div>
            </div>

            <div id="bg-service-2" class="box-one-third light-text home-box">
                <div class="inner">
                    <h2 class="wow fadeIn" data-wow-delay=".6s">Consolidación de Paquetes</h2>
                    <p class="wow fadeIn" data-wow-delay=".7s">Nos encargamos de consolidar tus compras en el menor volumen posible de envío, para así economizarle a nuestros clientes gastos elevados de envío y al mismo tiempo esos paquetes pequeños recibirlos en un solo envío.</p>
                    {{--<div class="divider-single"></div>--}}
                    {{--<a href="service-details-3.html" class="btn-border-light wow fadeInUp" data-wow-delay=".6s" data-wow-duration=".3s">Read More</a>--}}
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </section>

    <!-- section begin -->
    <section id="section-tracking">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form action="/rastreo-de-paquetes">
                        <div class="cta-form wow fadeIn" data-wow-delay="0s" data-wow-duration="1s">
                            <input type="text" name="track" value="" placeholder="Ingresa tu Número de Guía..." required >
                            <input type="submit" value="RASTREAR PAQUETE">
                            <div class="clearfix"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div id="section-tracking-result" class="light-text">
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-8 col-md-offset-2">--}}
                        {{--<div class="divider-double"></div>--}}
                        {{--<div class="text-center">--}}
                            {{--<h3><span class="grey">Product ID:</span> 112345679087328</h3>--}}
                        {{--</div>--}}


                        {{--<div class="divider-double"></div>--}}

                        {{--<div class="wrapper-line padding40 rounded10">--}}


                            {{--<ul class="progress">--}}
                                {{--<li><a href="">Accepted</a></li>--}}
                                {{--<li class="beforeactive"><a href="">Order Processing</a></li>--}}
                                {{--<li class="active"><a href="">Shipment Pending</a></li>--}}
                                {{--<li><a href="">Estimated Delivery</a></li>--}}
                            {{--</ul>--}}

                            {{--<div class="divider-double"></div>--}}

                            {{--<ul class="timeline custom-tl">--}}

                                {{--<li class="timeline-inverted">--}}
                                    {{--<div class="timeline-date wow zoomIn" data-wow-delay=".2s">--}}
                                        {{--Nov 03, 2015--}}
                                        {{--<span>20:07 pm</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="timeline-badge success"><i class="fa fa-check-square-o wow zoomIn"></i></div>--}}
                                    {{--<div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">--}}
                                        {{--<div class="timeline-body">--}}
                                            {{--The shipment has been successfully delivered--}}
                                            {{--<span class="location">Baker Street, UK <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="popup-gmaps">view on map</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                                {{--<li class="timeline-inverted">--}}
                                    {{--<div class="timeline-date wow zoomIn" data-wow-delay=".2s">--}}
                                        {{--Nov 03, 2015--}}
                                        {{--<span>20:07 pm</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="timeline-badge warning"><i class="fa fa-warning wow zoomIn"></i></div>--}}
                                    {{--<div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">--}}
                                        {{--<div class="timeline-body">--}}
                                            {{--The shipment could not be delivered--}}
                                            {{--<span class="location">Baker Street, UK <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="popup-gmaps">view on map</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                                {{--<li class="timeline-inverted">--}}
                                    {{--<div class="timeline-date wow zoomIn" data-wow-delay=".2s">--}}
                                        {{--Nov 03, 2015--}}
                                        {{--<span>20:07 pm</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="timeline-badge"><i class="fa fa-plane wow zoomIn"></i></div>--}}
                                    {{--<div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">--}}
                                        {{--<div class="timeline-body">--}}
                                            {{--The shipment has arrived in destination country--}}
                                            {{--<span class="location">Baker Street, UK <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="popup-gmaps">view on map</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                                {{--<li class="timeline-inverted">--}}
                                    {{--<div class="timeline-date wow zoomIn" data-wow-delay=".2s">--}}
                                        {{--Nov 02, 2015--}}
                                        {{--<span>18:05 pm</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="timeline-badge"><i class="fa fa-plane wow zoomIn"></i></div>--}}
                                    {{--<div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">--}}
                                        {{--<div class="timeline-body">--}}
                                            {{--The shipment is being transformed to destination country--}}
                                            {{--<span class="location">Baker Street, UK <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="popup-gmaps">view on map</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}

                                {{--<li class="timeline-inverted">--}}
                                    {{--<div class="timeline-date wow zoomIn" data-wow-delay=".2s">--}}
                                        {{--Nov 01, 2015--}}
                                        {{--<span>10:08 pm</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="timeline-badge"><i class="fa fa-plane wow zoomIn"></i></div>--}}
                                    {{--<div class="timeline-panel wow fadeInRight" data-wow-delay=".6s">--}}
                                        {{--<div class="timeline-body">--}}
                                            {{--The international shipment has been processed--}}
                                            {{--<span class="location">Baker Street, UK <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&amp;hl=en&amp;t=v&amp;hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="popup-gmaps">view on map</a></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}


                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>

    </section>
    <!-- section close -->

    @endsection