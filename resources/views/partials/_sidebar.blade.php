<ul class="list-group">
    <li class="list-group-item">
        <a href="{{ action('HomeController@index') }}">
            <i class="fa fa-home"></i> Inicio
        </a>
    </li>

    @if( !Auth::user()->isAdmin() )

    <li class="list-group-item">
        <a href="{{ action('BoxesController@index') }}"><i class="fa fa-archive"></i> Mis Envíos</a>
    </li>
    <li class="list-group-item">
        <a href="{{ action('BoxesController@create') }}"><i class="fa fa-plus-circle"></i> Crear Envío</a>
    </li>

    @else
        
    <li class="list-group-item">
        <a href="{{ action('AdminController@users') }}"><i class="fa fa-lock"></i> Reporte de Usuarios</a>
    </li>
    <li class="list-group-item">
        <a href="{{ action('AdminController@boxes') }}"><i class="fa fa-lock"></i> Reporte de Paquetes</a>
    </li>

    @endif

    <li class="list-group-item">
        <a href="{{ action('UsersController@password') }}"><i class="fa fa-key"></i> Cambiar Contraseña</a>
    </li>
    <li class="list-group-item">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i> Salir
        </a>
    </li>

</ul>