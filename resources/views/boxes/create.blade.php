@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <form action="{{ action('BoxesController@store') }}" method="post" onsubmit="return confirm('¿Todo confirmado?')"  >
                    {{ csrf_field() }}
                    <div class="panel panel-primary">
                        <div class="panel-heading">Crear Nuevo Envío</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }} {{ Auth::user()->last_name }}" required >
                            </div>
                            <div class="form-group">
                                <label for="address">Dirección de Envío</label>
                                <input type="text" class="form-control" name="address" value="{{ Auth::user()->address }}" placeholder="¿En donde quieres recibir tu paquete?" required >
                            </div>
                            <div class="form-group">
                                <label for="address2">Bloque / Apartamento / Manzana</label>
                                <input type="text" class="form-control" name="address2" value="{{ Auth::user()->address2 }}">
                            </div>
                            <div class="form-group">
                                <label for="city">Ciudad</label>
                                <input type="text" class="form-control" name="city" value="{{ Auth::user()->city }}" required >
                            </div>
                            <button class="btn btn-success">
                                <i class="fa fa-save"></i> Guardar Cambios
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection