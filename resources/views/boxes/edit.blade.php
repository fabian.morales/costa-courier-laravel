@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Editar Envio: {{ $box->name }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{ action('BoxesController@update', $box->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control" name="name" value="{{ $box->name }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Dirección de Envío</label>
                                        <input type="text" class="form-control" name="address" value="{{ $box->address }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Bloque / Apartamento / Manzana</label>
                                        <input type="text" class="form-control" name="address2" value="{{ $box->address2 }}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="city">Ciudad</label>
                                        <input type="text" class="form-control" name="city" value="{{ $box->city }}" required >
                                    </div>
                                    <button class="btn btn-success form-control">
                                        <i class="fa fa-save"></i> Guardar Cambios
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <p class="lead">¿Cómo funciona?</p>
                                <p>Una vez creas tu envío, puedes agregar paquetes con distintos números de guía para que tengas control de lo que quieres acumular en un solo envío. Así, te damos flexibilidad para que puedas ahorrar dinero mientras que envías tus compras de manera confiable con Costa Courier.</p>
                            </div>
                        </div>
                    </div>
                </div>
                @if( count($box->packages) > 0 )
                <p>
                    <a onclick="return confirm('Estás seguro de que los paquetes asociados con este envío están completos y deseas ordenar el despacho de los mismos?')" href="{{ action('BoxesController@delivery_request', $box->id) }}" class="btn btn-danger form-control">
                        SOLICITAR DESPACHO DE ESTE ENVÍO
                    </a>
                </p>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Administrar Paquetes
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <form action="{{ action('PackagesController@store', $box->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="tracking_code">Número de Guía</label>
                                        <input type="text" class="form-control" name="tracking_code" value="{{ old('tracking_code') }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="content">Contenido</label>
                                        <textarea name="content" id="content" cols="30" rows="5"
                                                  class="form-control" maxlength="120">{{ old('content') }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="content_price">Valor del Contenido</label>
                                        <input type="text" class="form-control" name="content_price" required >
                                    </div>
                                    <button class="btn btn-success form-control">
                                        <i class="fa fa-save"></i> Guardar Cambios
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-8">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Número de Guía</th>
                                            <th>Contenido</th>
                                            <th>Vr. Contenido</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($box->packages as $package)
                                        <tr>
                                            <td>{{ $package->tracking_code }}</td>
                                            <td>{{ $package->content }}</td>
                                            <td>{{ $package->content_price }}</td>
                                            <td class="text-center">
                                                <form action="{{ action('PackagesController@destroy', $package->id) }}" method="post" onsubmit="return confirm('Está seguro?')">
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection