@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">Mis Envíos</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Código de Rastreo</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Estado</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($boxes as $box)
                                    <tr>
                                        <th>{{ $box->id }}</th>
                                        <th>{{ $box->name }}</th>
                                        <td>{{ $box->tracking_code }}</td>
                                        <td>{{ $box->address }}</td>
                                        <td>{{ $box->city }}</td>
                                        <td><span class="badge">{{ $box->status }}</span></td>
                                        <td>
                                            <a href="{{ action('BoxesController@edit', $box->id) }}">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $boxes->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection