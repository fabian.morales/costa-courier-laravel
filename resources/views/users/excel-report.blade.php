<table>
    <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>DOCUMENTO</th>
        <th>TELEFONO</th>
        <th>DIRECCION</th>
        <th>DIRECCION 2</th>
        <th>CIUDAD</th>
        <th>EMAIL</th>
    </tr>
    @foreach($boxes as $box)
    <tr>
        <td>{{ $box->id }}</td>
        <td>{{ $box->name }}</td>
        <td>{{ $box->last_name }}</td>
        <td>{{ $box->document_number }}</td>
        <td>{{ $box->phone }}</td>
        <td>{{ $box->address }}</td>
        <td>{{ $box->address2 }}</td>
        <td>{{ $box->city }}</td>
        <td>{{ $box->email }}</td>
    </tr>
    @endforeach

</table>