@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Administrar Contraseña
                    </div>
                    <div class="panel-body">
                        <form action="{{ action('UsersController@password_update') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="old_password">Contraseña Anterior</label>
                                <input type="password" class="form-control" name="old_password" required >
                            </div>
                            <div class="form-group">
                                <label for="password">Nueva Contraseña</label>
                                <input type="password" class="form-control" name="password" required >
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">Confirmar Contraseña</label>
                                <input type="password" class="form-control" name="password_confirmation" required >
                            </div>
                            <button class="btn btn-success">
                                <i class="fa fa-save"></i> Guardar Cambios
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection