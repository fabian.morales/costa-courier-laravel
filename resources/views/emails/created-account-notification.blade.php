@extends('layouts.email')

@section('content')

    <p>A continuación la información relacionada:</p>

    <p>
        <b>Casillero No.</b><br />
        {{ $user->id }}
    </p>

    <p>
        <b>Nombre</b><br />
        {{ $user->name }}
    </p>

    <p>
        <b>Email</b><br />
        {{ $user->email }}
    </p>

    <p>
        <b>Cuenta creada</b><br />
        {{ $user->created_at }}
    </p>

    @endsection