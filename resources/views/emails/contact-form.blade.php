@extends('layouts.email')

@section('content')

    <p>A continuación la información relacionada:</p>

    @foreach($request->toArray() as $field => $value )
    <p>
        <b>{{ $field }}</b>
        <br>{{ $value }}
    </p>
    @endforeach

    @endsection