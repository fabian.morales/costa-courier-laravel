@extends('layouts.email')

@section('content')

    <p>A continuación la información relacionada:</p>

    <p>
        <b>Nombre:</b> {{ $box->name }}<br />
        <b>Dirección:</b> {{ $box->address }} | {{ $box->address2 }}<br />
        <b>Ciudad:</b> {{ $box->city }}<br />
        <b>Estado:</b> {{ $box->status }}<br />
    </p>

    <hr>

    <p>
        <b>Información del Casillero</b>
    </p>

    <p>
        <b># Casillero:</b> {{ $box->user->id }}<br />
        <b>Nombre:</b> {{ $box->user->name }}<br />
        <b>Apellido:</b> {{ $box->user->last_name }}<br />
        <b>Email:</b> {{ $box->user->email }}<br />
        <b>Documento:</b> {{ $box->user->document_number }}<br />
        <b>Teléfono:</b> {{ $box->user->phone }}<br />
        <b>Dirección:</b> {{ $box->user->address }}, {{ $box->user->address }}<br />
        <b>Ciudad:</b> {{ $box->user->city }}<br />
    </p>

    <p>
        <b>Paquetes Asociados Al Envio</b>
    </p>

    @foreach($box->packages as $package)

        <hr>
        <p>
            <b>Tracking:</b> {{ $package->tracking_code }}<br />
            <b>Contenido:</b> {{ $package->content }}
        </p>

    @endforeach

    @endsection