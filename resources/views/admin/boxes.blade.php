@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Envíos
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Dirección</th>
                                    <th>Ciudad</th>
                                    <th>Estado</th>
                                    <th>Usuario</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $boxes as $box )
                                    <tr>
                                        <th>
                                            <a href="{{ action('AdminController@box', $box->id) }}">
                                                {{ $box->name }}
                                            </a>
                                        </th>
                                        <td>{{ $box->address }}<br />{{ $box->address2 }}</td>
                                        <td>{{ $box->city }}</td>
                                        <td>{{ $box->status }}</td>
                                        <td>{{ $box->user->name }}</td>
                                        <td class="text-center">
                                            <a href="{{ action('AdminController@box', $box->id) }}">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $boxes->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection