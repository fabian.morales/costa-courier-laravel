@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Envío No. {{ $box->id }}
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>{{ $box->name }}</td>
                                </tr>
                                <tr>
                                    <td>Dirección</td>
                                    <td>{{ $box->address }}<br />{{ $box->address2 }}</td>
                                </tr>
                                <tr>
                                    <td>Ciudad</td>
                                    <td>{{ $box->city }}</td>
                                </tr>
                                <tr>
                                    <td>Estado</td>
                                    <td>{{ $box->status }}</td>
                                </tr>
                                <tr>
                                    <td>Usuario</td>
                                    <td>
                                        <a href="{{ action('AdminController@user', $box->user->id) }}">
                                            {{ $box->user->name }} {{ $box->user->last_name }}
                                        </a>
                                    </td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Paquetes
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Tracking</th>
                                    <th>Contenido</th>
                                    <th>Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($box->packages as $package)
                                <tr>
                                    <td>{{ $package->tracking_code }}</td>
                                    <td>{{ $package->content }}</td>
                                    <td>{{ $package->content_price }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection