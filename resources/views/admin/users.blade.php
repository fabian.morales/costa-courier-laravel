@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <p class="text-right">
                    <a href="{{ action('AdminController@excel_report') }}" class="btn btn-success">
                        Exportar Usuarios
                    </a>
                </p>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Reporte de Usuarios
                    </div>
                    <div class="panel-body">
                        <form action="">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" placeholder="Buscar..." >
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>Documento</th>
                                    <th>Teléfono</th>
                                    <th># Envíos</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $users as $user )
                                    <tr>
                                        <td>
                                            <a href="{{ action('AdminController@user', $user->id) }}">
                                                {{ $user->name }}
                                            </a>
                                        </td>
                                        <td>{{ $user->last_name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->document_number }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td class="text-center">
                                            <span class="label label-success">
                                                {{ count($user->boxes) }}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{ action('AdminController@user', $user->id) }}">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $users->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection