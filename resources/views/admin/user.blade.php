@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Detalles del Usuario: {{ $user->name }} {{ $user->last_name }}
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed table-striped">
                                <tr>
                                    <th>Nombre</th>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Apellido</th>
                                    <td>{{ $user->last_name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Documento</th>
                                    <td>{{ $user->document_number }}</td>
                                </tr>
                                <tr>
                                    <th>Teléfono</th>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td>{{ $user->address }}<br />{{ $user->address2 }}</td>
                                </tr>
                                <tr>
                                    <th>Ciudad</th>
                                    <td>{{ $user->city }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <p class="lead">Envios</p>
                <hr>
                @foreach($user->boxes as $box)
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            {{ $box->name }}
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-condensed">
                                            <tr>
                                                <th>Dirección</th>
                                                <td>{{ $box->address }}<br />{{ $box->address2 }}</td>
                                            </tr>
                                            <tr>
                                                <th>Ciudad</th>
                                                <td>{{ $box->city }}</td>
                                            </tr>
                                            <tr>
                                                <th>Estado</th>
                                                <td>{{ $box->status }}</td>
                                            </tr>
                                            <tr>
                                                <th>Fecha de Creación</th>
                                                <td>{{ $box->created_at }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Paquetes</div>
                                        <div class="panel-body">
                                            <table class="table table-striped table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Tracking</th>
                                                    <th>Contenido</th>
                                                    <th>Valor</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach( $box->packages as $package )
                                                    <tr>
                                                        <td>{{ $package->tracking_code }}</td>
                                                        <td>{{ $package->content }}</td>
                                                        <td>{{ $package->content_price }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-3">
                @include('partials._sidebar')
            </div>
        </div>
    </div>

    @endsection