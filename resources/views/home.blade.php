@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <p class="lead">
                <b>¡Tu casillero en Miami está listo!</b><br />
                Sigue estos sencillos pasos para disfrutar de tus compras:
            </p>
            <div class="panel panel-primary">
                <div class="panel-heading">1. Realiza tus Compras y Envíalas a tu Casillero Personalizado</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step1.png" alt="Compra" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Ingresa a tus sitios favoritos y realiza tus compras. Cuando estés finalizando el proceso envíalos a la siguiente dirección:</p>
                            <p class="lead text-center">
                                5502 NW 79 Ave, Miami, FL 33166<br />
                                Suite {{ Auth::user()->id }}
                            </p>
                            <p class="text-center">No te olvides de enviarlas a nombre de <b style="text-transform: uppercase">{{ Auth::user()->name }} {{ Auth::user()->last_name }}</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">2. Crea tu Envío</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step2.png" alt="Crea" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Crea tu Envío en la Nuestra Plataforma indicando:</p>
                            <ul>
                                <li>Dirección en donde quieres recibir tus paquetes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">3. Agrega Tus Paquetes</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/package.png" alt="Crea" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Agrega los paquetes relacionados con tu envío indicando:</p>
                            <ul>
                                <li>Tracking Number</li>
                                <li>Contenido</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">3. Espera Nuestra Confirmación</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step3.png" alt="Crea" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Una vez tus paquetes se encuentren en nuestra bodega, recibirás una confirmación de llegada y una liquidación final del costo de envío de tus paquetes.</p>
                            <p>No te olvides que podemos realizar tus envíos a cualquier lugar en Colombia.</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">4. Realiza el Pago del Envío</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step4.png" alt="Crea" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Utiliza cualquiera de nuestros medios de pago:</p>
                            <ul>
                                <li>Consignación en Davivienda</li>
                                <li>Pago con Tarjeta de Crédito vía telefónica</li>
                                <li>Deposito CHASE Bank, en Estados Unidos</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">5. Recibe tus Paquetes</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step5.png" alt="Crea" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <ul>
                                <li>Entrega de 5 a 7 días hábiles</li>
                                <li>Envíos a través de TCC y Coordinadora Mercantil</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">6. ¡Comienza de Nuevo!</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-3">
                            <img src="/img/step6.png" alt="Repite" class="img-responsive">
                        </div>
                        <div class="col-xs-9">
                            <p>Nuestra larga trayectoria, cumplimiento y responsabilidad te garantizan que puedas hacer el proceso una y otra y otra vez.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('partials._sidebar')
        </div>
    </div>
</div>
@endsection
