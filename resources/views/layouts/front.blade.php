<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WTNNB74');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <title>Envía Rápido y Seguro con Costa Courier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- LOAD CSS FILES -->
    <link href="/css/main.css" rel="stylesheet" type="text/css">

    <script type="text/javascript"> //<![CDATA[
        var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
        document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
        //]]>
    </script>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTNNB74"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="preloader"></div>
<div id="wrapper">

    <!-- header begin -->
    <header>
        <div class="container">
            <span id="menu-btn"></span>
            <div class="row">
                <div class="col-md-2">

                    <!-- logo begin -->
                    <div id="logo">
                        <div class="inner">
                            <a href="{{ URL::to('') }}">
                                <img src="img/logo.png" alt="{{ config('app.name') }}" class="logo-1">
                                <img src="img/logo-2.png" alt="{{ config('app.name') }}" class="logo-2">
                            </a>

                        </div>
                    </div>
                    <!-- logo close -->

                </div>

                <div class="col-md-10">

                    <!-- mainmenu begin -->
                    <nav id="mainmenu-container">
                        <ul id="mainmenu">
                            <li><a href="{{ URL::to('') }}"><i class="fa fa-home hidden-xs"></i> <span class="visible-xs">Inicio</span></a></li>
                            <li><a href="{{ action('PagesController@shipping') }}">Envíos</a></li>
                            <li><a href="{{ action('PagesController@calculator') }}">Calculadora</a></li>
                            <li><a href="{{ action('PagesController@tracking') }}">Rastreo de Paquetes</a></li>
                            <li><a href="{{ action('PagesController@faq') }}">Preguntas Frecuentes</a></li>
                            <li><a href="{{ action('PagesController@blog') }}">Blog</a></li>
                            <li><a href="{{ action('PagesController@contact') }}">Contacto</a></li>
                            <li><a href="/login"><i class="fa fa-user hidden-xs"></i> <span class="visible-xs">Acceso de Usuarios</span></a></li>
                        </ul>
                    </nav>
                    <!-- mainmenu close -->

                    <!-- social icons -->
                    {{--<div class="social">--}}
                        {{--<a href="https://www.facebook.com/costacourier/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a>--}}
                        {{--<a href="#"><i class="fa fa-twitter"></i></a>--}}
                        {{--<a href="#"><i class="fa fa-instagram"></i></a>--}}
                    {{--</div>--}}
                    <!-- social icons close -->

                </div>
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="container">
                <div class="alert alert-danger" style="margin-top: 15px">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        @if ( Session::has('message') )
            <div class="container">
                <div class="alert alert-{{ Session::get('message')['type'] }}" style="margin-top: 15px">
                    {{ Session::get('message')['message'] }}
                </div>
            </div>
        @endif
    </header>
    <!-- header close -->

    @yield('content')



<!-- section begin -->
    <section id="section-partners">
        <div class="container">
            <div class="row">

                <div class="logo-partners">
                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay=".2s" data-wow-duration=".6s" src="/img/logo/amazon.png" alt="">
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay=".5s" data-wow-duration=".5s" src="/img/logo/apple.png" alt="">
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay=".8s" data-wow-duration=".5s" src="/img/logo/ebay.png" alt="">
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay="1.1s" data-wow-duration=".5s" src="/img/logo/macys.png" alt="">
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay="1.4s" data-wow-duration=".5s" src="/img/logo/nike.png" alt="">
                    </div>

                    <div class="col-md-2 col-xs-4">
                        <img class="wow fadeInRight" data-wow-delay="1.7s" data-wow-duration=".5s" src="/img/logo/tjmaxx.png" alt="">
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <!-- section close -->

    @if( Request::url() != action('PagesController@faq') )

    <!-- section begin -->
    <section id="section-faq" class="light-text hidden-xs">

        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h2 class="wow fadeInUp" data-wow-delay="0">
                        Preguntas Frecuentes
                    </h2>
                    <div class="small-border wow flipInY" data-wow-delay=".2s" data-wow-duration=".8s"></div>
                </div>

                <div class="col-md-12 wow fadeInLeft" data-wow-delay=".5s" data-duration=".75s">

                    <ul class="accordion">

                        @foreach(array_slice($faqs, 0, 3) as $faq)

                        <li>
                            <a class="Active">{{ $faq['question'] }}</a>
                            <div class="content">
                                {!! $faq['answer'] !!}
                            </div>
                        </li>

                        @endforeach

                    </ul>

                </div>

                <div class="divider-single"></div>

                <div class="text-center">
                    <a href="{{ action('PagesController@faq') }}" class="btn-border-light wow fadeInUp" data-wow-delay="0">Ver todas las preguntas</a>
                </div>


            </div>
        </div>

        <div class="clearfix"></div>
    </section>
    <!-- section close -->

    @endif

    <div class="call-to-action text-light">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2>¡Crea tu cuenta gratís hoy mismo!</h2>
                </div>

                <div class="col-md-3">
                    <a href="/register" class="btn-border-light">Registrarme</a>
                </div>
            </div>
        </div>
    </div>

    <!-- footer begin -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="spacer-single"></div>
                    <p class="text-center">
                        &copy; Copyright {{ date('Y') }} - Costa Courier | Powered By <a
                                href="https://closerdesign.net">Closer Design Networks</a>
                    </p>
                    <p class="text-center">
                        <script language="JavaScript" type="text/javascript">
                            TrustLogo("https://costacourier.com/img/comodo_secure_seal_76x26_transp.png", "CL1", "none");
                        </script>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer close -->
</div>

<!-- LOAD JS FILES -->
<script async src="/js/jquery.min.js"></script>
<script async src="/js/bootstrap.min.js"></script>
<script async src="/js/jquery.isotope.min.js"></script>
<script async src="/js/easing.js"></script>
<script async src="/js/jquery.ui.totop.js"></script>
<script async src="/js/ender.js"></script>
<script async src="/js/owl.carousel.js"></script>
<script async src="/js/jquery.fitvids.js"></script>
<script async src="/js/jquery.plugin.js"></script>
@if(\Illuminate\Support\Facades\Request::url() == action('PagesController@contact'))
<script src="/js/contact.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0OPPvydyoMVHfVCnncb16m9rlflWeSco&v=3.exp"></script>
<script src="/js/map.js"></script>
@endif
<script async src="/js/wow.min.js"></script>
<script async src="/js/jquery.magnific-popup.min.js"></script>
<script async src="/js/jquery.stellar.js"></script>
<script async src="/js/typed.js"></script>
<script async src="/js/jquery.scrollto.js"></script>
<script async src="/bower_components/jQuery.equalHeights/jquery.equalheights.min.js"></script>
<script async src="/js/custom.js"></script>

<!-- SLIDER REVOLUTION SCRIPTS  -->
<script async type="text/javascript" src="/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script async type="text/javascript" src="/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script async src="/js/revslider-custom.js"></script>

<!-- snow fx -->
<script async src="/js/let-it-snow.min.js"></script>

@if( date('m' == 12) )

    <script>
        $.letItSnow('.let-it-snow', {
            stickyFlakes: 'lis-flake--js',
            makeFlakes: true,
            sticky: true
        });
    </script>

    @endif

</body>
</html>
