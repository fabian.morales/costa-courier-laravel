<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){

            $table->string('last_name');
            $table->string('document_number');
            $table->string('phone');
            $table->string('address');
            $table->string('address2')->nullable();
            $table->string('city');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table){

            $table->dropColumn('last_name');
            $table->dropColumn('document_number');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('address2');
            $table->dropColumn('city');

        });
    }
}
