<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('envios', 'PagesController@shipping');
Route::get('calculadora', 'PagesController@calculator');
Route::get('rastreo-de-paquetes', 'PagesController@tracking');
Route::get('preguntas-frecuentes', 'PagesController@faq');
Route::get('contacto', 'PagesController@contact');
Route::get('blog', 'PagesController@blog');
Route::get('blog/{id}', 'PagesController@post');

Route::post('contacto', 'FormsController@contact');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('boxes', 'BoxesController');
Route::get('boxes/delivery-request/{box}', 'BoxesController@delivery_request');
Route::resource('packages', 'PackagesController');
Route::post('packages/{box}', 'PackagesController@store');

Route::get('password', 'UsersController@password');
Route::post('password/update', 'UsersController@password_update');

Route::group(['prefix' => 'admin'], function (){

    Route::get('usuarios', 'AdminController@users');
    Route::get('usuario/{id}', 'AdminController@user');
    Route::get('envios', 'AdminController@boxes');
    Route::get('envio/{id}', 'AdminController@box');
    Route::get('reporte-temporal', 'AdminController@excel_report');

});