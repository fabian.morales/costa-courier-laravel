<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Box extends Model
{
    protected $fillable = [
        'name',
        'tracking_code',
        'contents',
        'address',
        'address2',
        'city',
        'status',
    ];

    public function scopeUsers()
    {
        return $this->where('user_id', Auth::user()->id);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function packages()
    {
        return $this->hasMany('App\Package');
    }
}
