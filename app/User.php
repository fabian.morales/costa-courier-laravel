<?php

namespace App;

use App\Events\NewUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'document_number', 'phone', 'address', 'address2', 'city', 'email', 'password', 'is_admin',
    ];

    protected $events = [
        'created'  => NewUser::class,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        $admin = false;

        if($this->is_admin == true)
        {
            $admin = true;
        }

        return $admin;
    }

    public function boxes()
    {
        return $this->hasMany('App\Box');
    }
}
