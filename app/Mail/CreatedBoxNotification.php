<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatedBoxNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $box;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($box)
    {
        $this->box = $box;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.created-box-notification')->subject('[' . $this->box->id . '] Nueva Solicitud De Despacho De Envíos');
    }
}
