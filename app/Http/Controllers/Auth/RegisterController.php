<?php

namespace App\Http\Controllers\Auth;

use App\Listeners\SignUpNewUser;
use App\Mail\CreatedAccountNotification;
use App\User;
use App\Http\Controllers\Controller;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'last_name' => 'required',
            'document_number' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'document_number' => $data['document_number'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'city' => $data['city'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        Sendy::setListId('oZpGSXZX3dHC4qzz763OGT0Q')->subscribe([
            'name' => $user->name,
            'email' => $user->email,
            'box' => $user->id,
        ]);

        Mail::to('shipping@costacourier.com')
            ->cc('costacourier@closerdesign.co')
            ->send(new CreatedAccountNotification($user));

        return $user;
    }
}
