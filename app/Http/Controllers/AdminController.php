<?php

namespace App\Http\Controllers;

use App\Box;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     *  Excel Report
     *
     *
     */

    public function excel_report()
    {
        $boxes = User::all();

        Excel::create('Costa Courier', function($excel) use ($boxes){

            $excel->sheet('Casilleros', function($sheet) use ($boxes){

                $sheet->loadView('users.excel-report')->with('boxes', $boxes);

            });

        })->download('xlsx');
    }

    /**
     *  Users
     *
     *
     */

    public function users()
    {
        if( isset( $_GET['keyword'] ) )
        {
            $users = User::where(function($q){
                $q->where('name', 'like', '%' . $_GET['keyword'] . '%')
                ->orWhere('last_name', 'like', '%' . $_GET['keyword'] . '%')
                ->orWhere('email', 'like', '%' . $_GET['keyword'] . '%')
                ->orWhere('document_number', 'like', '%' . $_GET['keyword'] . '%')
                ->orWhere('phone', 'like', '%' . $_GET['keyword'] . '%')
                ->orWhere('city', 'like', '%' . $_GET['keyword'] . '%');
            })->paginate(25);

        }else{

            $users = User::paginate(15);
        }

        return view('admin.users', compact('users'));
    }

    /**
     *  Single User
     *
     *
     */

    public function user($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user', compact('user'));
    }

    /**
     *  Boxes
     *
     *
     */

    public function boxes()
    {
        $boxes = Box::paginate(50);

        return view('admin.boxes', compact('boxes'));
    }

    /**
     *  Box
     *
     *
     */

    public function box($id)
    {
        $box = Box::findOrFail($id);

        return view('admin.box', compact('box'));
    }
}
