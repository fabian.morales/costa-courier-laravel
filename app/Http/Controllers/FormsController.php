<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FormsController extends Controller
{
    /**
     *  Contact Form
     *
     *
     */

    public function contact(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'phone'    => 'required',
            'email'    => 'required|email'
        ]);

        Sendy::subscribe([
            'name' => $request->name,
            'email' => $request->email
        ]);

        Mail::to('costacourier@closerdesign.co')->send(new ContactForm($request));

        Session::flash('message', [
            'type'     => 'success',
            'message'  => 'Gracias por escribirnos! Hemos recibido tu mensaje y pronto estaremos en contacto.'
        ]);

        return back();
    }
}
