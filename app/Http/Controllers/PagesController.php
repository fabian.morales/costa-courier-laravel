<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PagesController extends Controller
{
    private $faqs;

    public function __construct()
    {
        $this->faqs = [
            1 => [
                'question' => '¿Quisiera saber en cuantos dolares tienen la libra?',
                'answer'   => 'La libra tiene un costo de $1.70USD, el mínimo de envío son 15 libras + 30% valor declarado para pago de impuestos de aduana.'
            ],
            2 => [
                'question' => 'Ya me registre en la página pero solo me sale la dirección de ustedes en MIAMI, ¿debo colocar el nombre con el que me registre?',
                'answer'   => 'Despues de que usted se registre, sus datos quedaran en nuestra base de clientes. El nombre para sus órdenes debe ser el mismo con el que se registró.'
            ],
            3 => [
                'question' => '¿Para retirar los paquetes en Cúcuta, Puedo pagar con dólares efectivos?',
                'answer'   => 'Los retiros en Cúcuta únicamente se pueden pagar en pesos COP. Si desea pagar en dólares deberá comunicarse a Miami al 305-6299595 y pagar con una tarjeta en dólares.'
            ],
            4 => [
                'question' => 'Por favor me puede confirmar ¿cuáles son las tarifas vigentes para el envío de USA a Colombia?',
                'answer'   => 'Cualquier envío a Colombia a cualquier ciudad de destino el precio es de $1.70 USD libra mínimo 15 libras + 30% valor declarado para pago de impuestos de aduana.'
            ],
            5 => [
                'question' => '¿Pueden hacer despachos de productos sin factura o existe algún inconveniente al respecto?',
                'answer'   => 'Todo articulo ira con la factura que Costa Courier crea para el envío. No se usan facturas originales.'
            ],
            6 => [
                'question' => '¿Dónde puedo rastrear mis compras cuando sean enviadas a Colombia desde Miami?',
                'answer'   => 'Para rastrear sus compras una vez nosotros las hemos enviado a Colombia puede hacerlo a través de nuestro sitio web en la parte de “rastrea tus paquetes” con el número de rastreo que se le envía a cada cliente vía email con la información de "Aviso de Envío con el costo" Tracking #ZZ...'
            ],
            7 => [
                'question' => '¿Cuáles son las empresas que hacen entregas de mis compras en Colombia?',
                'answer'   => 'Las empresas que hacen las entregas en las distintas ciudades de Colombia son Coordinadora Mercantil y TCC.'
            ],
            8 => [
                'question' => '¿Cuantos días se demora la entrega en Colombia después que se despacha de Miami?',
                'answer'   => 'Una vez se despacha de Miami la entrega se puede tardar entre 5 y 7 días.'
            ],
            9 => [
                'question' => '¿Puedo pagar con tarjeta de crédito? ¿Cómo se haría el proceso de pago?',
                'answer'   => 'Usted tiene la opción de pagar con tarjeta de crédito llamándonos a Miami al 305-6299595 brindándonos el número de tarjeta y fecha de expiración, con estos datos y previa autorización podremos efectuar el pago.'
            ],
            10 => [
                'question' => '¿Cuánto es el peso mínimo de envío de ustedes y el máximo permitido de envío?',
                'answer'   => 'El peso mínimo de envío es de 15 Libras y el máximo para envío Courier son 100 libras en una sola pieza, más de ese peso seria envío de Manejo y el costo es distinto.'
            ],
            11 => [
                'question' => '¿Qué tipo de productos no se pueden enviar?',
                'answer'   => '
                    <p>Restricciones para envío de Correo Courier</p>
                    <p>Atendiendo lo previsto en el artículo 19 de la ley de 1978 las regulaciones para importaciones son las siguientes:</p>
                    <ul>
                        <li>El paquete no debe de pesar más de 110 libras.</li>
                        <li>Las medidas de las cajas no deben exceder, en suma las 60 pulgadas.</li>
                        <li>El contenido de este envío debe de ser: Artículos para el hogar, personales, muestras de todo aquello que no tenga regulaciones y su valor no exceda los $ 2,000 USD</li>
                        <li>Si el contenido son muestras, no debe haber más de 6 unidades de la misma referencia.</li>
                        <li>Opio, morfina, cocaína, estupefacientes sicotrópicos controlados, o no por el Ministerio de Salud.</li>
                        <li>Los medicamentos, por estar sujetos a Registro Sanitario y/o visto bueno previo de Invima.</li>
                        <li>Alimentos, plantas, animales vivos o muertos, sustancias o mezclas homeopáticas.</li>
                        <li>Armas, municiones, accesorios de guerra, prendas de uso privativo de las fuerzas militares, material peligroso.</li>
                        <li>Precursores químicos en la elaboración de estupefacientes, material radiactivo.</li>
                        <li>Objetos obscenos o inmorales, juguetes bélicos.</li>
                        <li>Monedas, billetes de banco, papel moneda o cualesquiera otros valores al portador, platino, oro o plata, manufacturados o no. Prenderías, alhajas y joyas.</li>
                        <li>Cada paquete tiene un seguro incluido de $200.00 por pérdida total.</li>
                        <li>Si desea, puede aumentar el seguro hasta un máximo de $2,000; el cargo adicional será de 3% del valor.</li>
                        <li>Todo paquete debe de ser revisado por el agente, pues es obligatorio ver su contenido.</li>
                    </ul>'
            ]
        ];
    }

    /**
     *  Home Page
     *
     *
     */

    public function index()
    {
        $faqs = $this->faqs;

        return view('pages.index', compact('faqs'));
    }

    /**
     *  Shipping
     *
     *
     */

    public function shipping()
    {
        $faqs = $this->faqs;

        return view('pages.shipping', compact('faqs'));
    }

    /**
     *  Calculator
     *
     *
     */

    public function calculator()
    {
        $faqs = $this->faqs;

        $total = 0;

        if( isset($_GET['lbs']) && isset($_GET['amount']) )
        {
            $lbs = 15;

            if($_GET['lbs'] > 15)
            {
                $lbs = $_GET['lbs'];
            }

            if($_GET['amount'] > 200)
            {
                $total = ($lbs * 2.3) + ($_GET['amount'] * 0.3);
            }else{
                $total = ($lbs * 2.3);
            }
        }

        return view('pages.calculator', compact('faqs', 'total'));
    }

    /**
     *  Tracking
     *
     *
     */

    public function tracking()
    {
        // 1z5771400367789671

        $faqs = $this->faqs;

        $statuses = [
            'departed' => 'DESPACHADO',
            'inProgress' => 'EN PROGRESO',
            'received' => 'RECIBIDO',
            'inTransit' => 'EN TRANSITO',
            'delivered' => 'ENTREGADO'
        ];

        $data = [];

        if(isset($_GET['track']))
        {
            try{

                $url = 'http://ecm-env.elasticbeanstalk.com/ecmlogic/tracking:track?biz=3&identifier=' . $_GET['track'];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
                $token = uniqid();
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest", "Content-Type: application/json; charset=utf-8", "__RequestVerificationToken: $token"));
                $result = curl_exec ($ch);

                curl_close ($ch);

                $data = (array)json_decode($result);

                $data = $data[0];

            } catch (\Exception $e)
            {
                Session::flash('message', [
                    'type'    => 'success',
                    'message' => 'No se han encontrado coincidencias'
                ]);

                return back();
            }
        }

        return view('pages.tracking', compact('data', 'statuses', 'faqs'));
    }

    /**
     *  FAQ
     *
     *
     */

    public function faq()
    {
        $faqs = $this->faqs;

        return view('pages.faqs', compact('faqs'));
    }

    /**
     *  Blog
     *
     *
     */

    public function blog()
    {
        $faqs = $this->faqs;

        $posts = 'http://closerdesign.net/api/posts/24';

        $posts = file_get_contents($posts);

        $posts = json_decode($posts);

        return view('pages.blog', compact('posts', 'faqs'));
    }

    /**
     *  Post
     *
     *
     */

    public function post($id)
    {
        $faqs = $this->faqs;

        $id = explode("-", $id)[0];

        $post = 'http://closerdesign.net/api/post/' . $id;

        $post = file_get_contents($post);

        $post = json_decode($post);

        return view('pages.post', compact('post', 'faqs'));
    }

    /**
     *  Contact Form
     *
     *
     */

    public function contact()
    {
        $faqs = $this->faqs;

        return view('pages.contact', compact('faqs'));
    }
}
