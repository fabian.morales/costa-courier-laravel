<?php

namespace App\Http\Controllers;

use App\Box;
use App\Mail\CreatedBoxNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class BoxesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes = Box::where('user_id', Auth::user()->id)->paginate(25);

        if(Auth::user()->isAdmin())
        {
            $boxes = Box::all();
        }

        return view('boxes.index', compact('boxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('boxes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'address'       => 'required',
            'city'          => 'required'
        ]);

        $box = new Box($request->all());

        $box->user_id = Auth::user()->id;

        $box->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Envío Almacenado Exitosamente'
        ]);

        return redirect()->action('BoxesController@edit', $box->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $box = Box::users()->findOrFail($id);

        return view('boxes.edit', compact('box'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Box Send Request
     *
     *
     */

    public function delivery_request($box)
    {
        $box = Box::findOrFail($box);

        Mail::to('shipping@costacourier.com')->send(new CreatedBoxNotification($box));

        Session::flash('message', [
            'type'     => 'success',
            'message'  => 'La Solicitud De Envío Se Ha Procesado De Manera Exitosa'
        ]);

        return back();
    }
}
