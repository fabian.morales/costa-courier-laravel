<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function password()
    {
        return view('users.password');
    }

    public function password_update(Request $request)
    {
        $this->validate($request, [
            'old_password'          => 'required',
            'password'              => 'required|confirmed'
        ]);

        if( Hash::check( $request->old_password, Auth::user()->password ) )
        {
            $user = User::findOrFail(Auth::user()->id);

            $user->password = bcrypt($request->password);

            $user->save();

            Session::flash('message', [
                'type'    => 'success',
                'message' => 'La contraseña ha sido actualizada!'
            ]);
        }else{
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'Su contraseña actual está errada. Por favor verifique.'
            ]);
        }

        return back();
    }
}
