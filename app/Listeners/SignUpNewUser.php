<?php

namespace App\Listeners;

use App\Events\NewUser;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SignUpNewUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUser  $event
     * @return void
     */
    public function handle(NewUser $event)
    {
//        Sendy::setListId('oZpGSXZX3dHC4qzz763OGT0Q')->subscribe([
//            'name' => $event->user->name,
//            'email' => $event->user->email,
//            'box' => $event->user->id
//        ]);
    }
}
